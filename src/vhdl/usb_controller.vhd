entity usb_controller is
  port (
    VDD: out bit;
    D_p, D_m: in bit;
    GND: out bit;
    clk_CI: in bit;
    reset_SI: in bit);
end;

architecture controll_logic of usb_controller is

  type T_STATE_KJ is (K, J);
  signal kj_state : T_STATE_KJ;
  
begin
  VDD <= D_p and D_m;

  process( clk_CI, reset_SI)
  begin
    if (reset_SI = '1')
    then kj_state <= K;
    elsif (clk_CI = '1') and clk_CI'event
    then kj_state <= J;
         end if;  

  end process;
end controll_logic;


