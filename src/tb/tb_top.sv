module tb_top();
   import tb_pkg::*;
   
   
   // clk an dasyschronous reset
   logic clk_C = 'b1;
   logic Reset_RB = 'b0;

   usb_controller DUT ( .clk_ci (clk_C),
                        .reset_SI (Reset_RB));


   initial begin : timing_format
      $timeformat(-9, 0, "ns", 8);

   end : timing_format

   initial begin : clock_generator
      while (1) begin
         #CLK_HIGH clk_C = 1'b0;
         #CLK_LOW clk_C = 1'b1;
      end
   end : clock_generator

endmodule
