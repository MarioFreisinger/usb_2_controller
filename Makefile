.PHONY: simg, simc

# graphhical simulation
simg:
		mkdir -p ./_hdl/
		vlib _hdl/work-vsim
		vlog -sv ./src/tb/tb_top_pkg.sv ./src/tb/tb_top.sv
		vcom ./src/vhdl/usb_controller.vhd
		vsim _hdl/work-vsim.tb_top

# graphhical simulation
simc:
		mkdir -p ./_hdl/
		vlib _hdl/work-vsim
		vlog -sv ./src/tb/tb_top_pkg.sv ./src/tb/tb_top.sv
		vcom ./src/vhdl/usb_controller.vhd
		vsim -c _hdl/work-vsim.tb_top
